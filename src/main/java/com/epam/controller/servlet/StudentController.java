package com.epam.controller.servlet;

import com.epam.model.Student;
import com.epam.util.CustomDatabase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

@WebServlet("/students/*")
public class StudentController extends HttpServlet {

    private static Logger log = LogManager.getLogger(StudentController.class);
    private String facultyType;

    @Override
    public void init() throws ServletException {
        log.info("Servlet " + getClass().getSimpleName() + " started");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        facultyType = req.getPathInfo().substring(1);
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        out.println("<h3>List of Student</h3>");
        getStudentsFromRequest(out);
        saveStudentForm(out);
        goToFaculties(out);
        deleteStudentForm(out);
        out.println("<p> <a href='../students/"+ facultyType + "'>REFRESH</a> </p>");
        out.println("</body></html>");
        resp.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("firstName");
        String secondName = req.getParameter("secondName");
        int age = Integer.parseInt(req.getParameter("age"));
        Student student = new Student(firstName, secondName, age);
        Objects.requireNonNull(CustomDatabase.getFacultyBy(facultyType)).get().addStudent(student);
        log.info("Student : " + firstName + " " + secondName +  "was created" );
        resp.setStatus(HttpServletResponse.SC_CREATED);
        doGet(req,resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getPathInfo().substring(1);
        Objects.requireNonNull(CustomDatabase.getFacultyBy(facultyType)).get().getStudents()
                .removeIf( s -> s.getId() == (Integer.parseInt(id)));
        log.info("Student with id " +  id  + " deleted");
        resp.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    public void destroy() {
        log.info("Servlet " + getClass().getSimpleName() + " destroyed");
    }

    private void getStudentsFromRequest(PrintWriter out){
        for (Student student :Objects.requireNonNull(CustomDatabase.getFacultyBy(facultyType)).get().getStudents()){
            out.println("<p>");
            out.println(student.getId() + " " +student.getFirstName() + " " +
                    student.getSecondName() + " " + student.getAge());
            out.println("</p>");
        }
    }

    private void saveStudentForm(PrintWriter out){
        out.println("<form action = \"/students/" + facultyType + "\"" + " method=\"post\">\n" +
                "    <input required type=\"text\" name=\"firstName\" placeholder=\"First Name\">\n" +
                "    <input required type=\"text\" name=\"secondName\" placeholder=\"Second Name\">\n" +
                "    <input required type=\"text\" name=\"age\" placeholder=\"Age\">\n" +
                "    <input type=\"submit\" value=\"Save Student\">\n" +
                "</form>");
    }

    private void deleteStudentForm(PrintWriter out){
        out.println("<form>\n"
                + "  <p><b>Delete Student</b></p>\n"
                + "  <p> Student id: <input type='text' name='student_id'>\n"
                + "  <input type='button' onclick='remove(this.form.student_id.value)' name='ok' "
                + "value='Delete Student'/>\n"
                + "  </p>\n"
                + "</form>");

        out.println("<script type='text/javascript'>\n"
                + "  function remove(id) { fetch(id, {method: 'DELETE'}); }\n"
                + "</script>");
    }

    private void goToFaculties(PrintWriter out){
        out.println("<p> <a href='../faculties'>Go to faculties</a> </p>");
    }
}
