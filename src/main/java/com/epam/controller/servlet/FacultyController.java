package com.epam.controller.servlet;

import com.epam.model.Faculty;
import com.epam.util.CustomDatabase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/faculties/*",
            name = "facultyServlet")
public class FacultyController extends HttpServlet {

    private static Logger log = LogManager.getLogger(FacultyController.class);

    @Override
    public void init() throws ServletException {
        log.info("Servlet " + getClass().getSimpleName() + " started");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        out.println("<h1>Welcome to Faculty Manager</h1>");
        out.println("<h3>List of Faculties</h3>");
        getFacultiesFromRequest(out);
        saveFacultyForm(out);
        deleteFacultyForm(out);
        out.println("<p> <a href='faculties'>REFRESH</a> </p>");
        resp.setStatus(HttpServletResponse.SC_OK);
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        Faculty faculty = new Faculty(name);
        CustomDatabase.addToFaculty(faculty);
        log.info("Faculty with name " + name + " created");
        resp.setStatus(HttpServletResponse.SC_CREATED);
        doGet(req,resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getPathInfo().substring(1);
        CustomDatabase.getFaculties().removeIf( f -> f.getId() == (Integer.parseInt(id)));
        log.info("Faculty with id " + id + " deleted");
        resp.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    public void destroy() {
        log.info("Servlet " + getClass().getSimpleName() + " destroyed");

    }

    private void getFacultiesFromRequest(PrintWriter out){
        for (Faculty faculty : CustomDatabase.getFaculties()){
            out.println("<p><a href=" +"students/" + faculty.getName().toLowerCase()+">");
            out.println(faculty.getId() + " " + faculty.getName() + " student list");
            out.println("</a></p>");
        }
    }

    private void deleteFacultyForm(PrintWriter out){
        out.println("<form>\n"
                + "  <p><b>Delete Faculty</b></p>\n"
                + "  <p> Faculty id: <input type='text' name='faculty_id'>\n"
                + "    <input type='button' onclick='remove(this.form.faculty_id.value)' name='ok' "
                + "value='Delete Faculty'/>\n"
                + "  </p>\n"
                + "</form>");

        out.println("<script type='text/javascript'>\n"
                + "  function remove(id) { fetch('faculties/' + id, {method: 'DELETE'}); }\n"
                + "</script>");
    }

    private void saveFacultyForm(PrintWriter out){
        out.println("<form action = \"/faculties\" method=\"post\">\n" +
                "    <input required type=\"text\" name=\"name\" placeholder=\"Faculty Name\">\n" +
                "    <input type=\"submit\" value=\"Save Faculty\">\n" +
                "</form>");
    }
}
