package com.epam.util;

import com.epam.model.Faculty;
import com.epam.model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CustomDatabase {

    private static List<Faculty> faculties;

    static {
        faculties = new ArrayList<>();
        Faculty biology = new Faculty("Biology");
        Faculty math = new Faculty("Math");
        Faculty computerScience = new Faculty("Computer");
        biology.addStudent(new Student("John", "Biology", 20));
        biology.addStudent(new Student("Jack", "Biology", 23));
        math.addStudent(new Student("John", "Math", 26));
        math.addStudent(new Student("Jack", "Math", 24));
        computerScience.addStudent(new Student("John", "Computer", 26));
        computerScience.addStudent(new Student("Jack", "Computer", 22));
        faculties.add(biology);
        faculties.add(math);
        faculties.add(computerScience);
    }

    public static List<Faculty> getFaculties() {
        return faculties;
    }

    public static void addToFaculty(Faculty faculty){
        faculties.add(faculty);
    }

    public static Optional<Faculty> getFacultyBy(String facultyType){
        for (Faculty faculty : getFaculties()){
            if (faculty.getName().equalsIgnoreCase(facultyType)){
                return Optional.of(faculty);
            }
        }
        return Optional.empty();
    }
}
