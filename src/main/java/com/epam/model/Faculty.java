package com.epam.model;

import java.util.ArrayList;
import java.util.List;

public class Faculty {

    private static int count = 1;
    private int id;
    private String name;
    private List<Student> students;

    public Faculty(String name) {
        this.name = name;
        id = count++;
        students = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void addStudent(Student student) {
        this.students.add(student);
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "name='" + name + '\'' +
                ", students=" + students +
                '}';
    }
}
